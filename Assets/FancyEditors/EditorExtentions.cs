﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class EditorExtentions
{
    public static object GetDefault(this Type type)
    {
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
            var valueProperty = type.GetProperty("Value");
            type = valueProperty.PropertyType;
        }

        return Activator.CreateInstance(type);
    }
}
