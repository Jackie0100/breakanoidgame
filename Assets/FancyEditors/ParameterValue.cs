﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParameterValue<T> : UnityEngine.Object
{
    [SerializeField]
    T _value;
}
