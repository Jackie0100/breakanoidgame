﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System;

// Place this file in any folder that is or is a descendant of a folder named "Editor"

[CanEditMultipleObjects] // Don't ruin everyone's day
[CustomEditor(typeof(MonoBehaviour), true)] // Target all MonoBehaviours and descendants
public class MonoBehaviourCustomEditor : UnityEditor.Editor
{
    Dictionary<string, object[]> values;

    private void Awake()
    {
        values = new Dictionary<string, object[]>();

        var type = target.GetType();

        // Iterate over each private or public instance method (no static methods atm)
        foreach (var method in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
        {
            var attributes = method.GetCustomAttributes(typeof(ButtonAttribute), true);
            if (attributes.Length > 0)
            {
                List<object> temp = new List<object>();
                foreach (var par in method.GetParameters())
                {
                    Debug.Log(par.ParameterType);
                    temp.Add(par.ParameterType.GetDefault());
                }
                values[method.Name + string.Concat(method.GetParameters().Select(t => t.Name))] = temp.ToArray();
            }
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector(); // Draw the normal inspector

        // Currently this will only work in the Play mode. You'll see why
        if (Application.isPlaying)
        {
            // Get the type descriptor for the MonoBehaviour we are drawing
            var type = target.GetType();

            // Iterate over each private or public instance method (no static methods atm)
            foreach (var method in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                // make sure it is decorated by our custom attribute
                var attributes = method.GetCustomAttributes(typeof(ButtonAttribute), true);
                if (attributes.Length > 0)
                {
                    var parameters = method.GetParameters();
                    for (int i = 0; i < parameters.Count(); i++)
                    {
                        Debug.Log(parameters[i].ParameterType);
                        values[method.Name + string.Concat(method.GetParameters().Select(t => t.Name))][i] = DrawEditorFromType(parameters[i].ParameterType, parameters[i].Name, values[method.Name + string.Concat(method.GetParameters().Select(t => t.Name))][i]);
                        //EditorGUILayout.(par.Name, new Object(), par.ParameterType);
                    }
                    if (GUILayout.Button("Run: " + method.Name))
                    {
                        // If the user clicks the button, invoke the method immediately.
                        // There are many ways to do this but I chose to use Invoke which only works in Play Mode.
                        method.Invoke((MonoBehaviour)target, values[method.Name + string.Concat(method.GetParameters().Select(t => t.Name))]);
                        //((MonoBehaviour)target).Invoke(method.Name, 0f);
                    }
                }
            }
        }
    }

    public object DrawEditorFromType(Type variabletype, string name, object value)
    {
        if (variabletype == typeof(int))
        {
            return EditorGUILayout.IntField(name, (int)value);
        }
        else if (variabletype == typeof(long))
        {
            return EditorGUILayout.LongField(name, (long)value);
        }
        else if (variabletype == typeof(float))
        {
            return EditorGUILayout.FloatField(name, (float)value);
        }
        else if (variabletype == typeof(double))
        {
            return EditorGUILayout.DoubleField(name, (double)value);
        }
        else if (variabletype == typeof(string))
        {
            return EditorGUILayout.TextField(name, (string)value);
        }
        else if (variabletype == typeof(bool))
        {
            return EditorGUILayout.Toggle(name, (bool)value);
        }
        else if (variabletype == typeof(Vector2))
        {
            return EditorGUILayout.Vector2Field(name, (Vector2)value);
        }
        else if (variabletype == typeof(Vector3))
        {
            return EditorGUILayout.Vector3Field(name, (Vector3)value);
        }
        else if (variabletype == typeof(UnityEngine.Object))
        {
            return EditorGUILayout.ObjectField(name, (UnityEngine.Object)value, variabletype, true);
        }
        else
        {
            //var fields = value.GetType().GetFields();
            //List<SerializedProperty> sos = new List<SerializedProperty>();

            //Type type = typeof(ParameterValue<>);
            //Type gentype = type.MakeGenericType(variabletype);
            
            //var obj = Activator.CreateInstance(gentype);
            //SerializedObject so = new SerializedObject((UnityEngine.Object)obj);

            //foreach (var field in fields)
            //{
            //    if (field.GetCustomAttribute<SerializeField>() != null)
            //    {
            //        //EditorGUILayout.PropertyField( field.Name, new GUIContent("Int Field"), GUILayout.Height(20));
            //    }
            //}

            //Editor.(value).CreateInspectorGUI();
            return value;
        }
    }
}

