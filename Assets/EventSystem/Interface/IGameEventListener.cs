﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IGameEventListener
{
    void OnEventRaised();
}

public interface IGameEventListener<in T>
{
    void OnEventRaised(T value);
}