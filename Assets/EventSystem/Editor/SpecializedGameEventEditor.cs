﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BoolGameEvent))]
public class BoolGameEventEditor : Editor
{
    bool _checked;
    public override void OnInspectorGUI()
    {
        _checked = EditorGUILayout.Toggle("Event Value", _checked);
        if (GUILayout.Button("Execute Events"))
        {
            ((BoolGameEvent)target).Invoke(_checked);
        }
    }
}

[CustomEditor(typeof(FloatGameEvent))]
public class FloatGameEventEditor : Editor
{
    float _value;
    public override void OnInspectorGUI()
    {
        _value = EditorGUILayout.FloatField("Event Value", _value);
        if (GUILayout.Button("Execute Events"))
        {
            ((FloatGameEvent)target).Invoke(_value);
        }
    }
}


[CustomEditor(typeof(IntGameEvent))]
public class IntGameEventEditor : Editor
{
    int _value;
    public override void OnInspectorGUI()
    {
        _value = EditorGUILayout.IntField("Event Value", _value);
        if (GUILayout.Button("Execute Events"))
        {
            ((IntGameEvent)target).Invoke(_value);
        }
    }
}


[CustomEditor(typeof(StringGameEvent))]
public class StringGameEventEditor : Editor
{
    string _value;
    public override void OnInspectorGUI()
    {
        _value = EditorGUILayout.TextField("Event Value", _value);
        if (GUILayout.Button("Execute Events"))
        {
            ((StringGameEvent)target).Invoke(_value);
        }
    }
}


[CustomEditor(typeof(Vector2GameEvent))]
public class Vector2GameEventEditor : Editor
{
    Vector2 _value;
    public override void OnInspectorGUI()
    {
        _value = EditorGUILayout.Vector2Field("Event Value", _value);
        if (GUILayout.Button("Execute Events"))
        {
            ((Vector2GameEvent)target).Invoke(_value);
        }
    }
}


[CustomEditor(typeof(Vector3GameEvent))]
public class Vector3GameEventEditor : Editor
{
    Vector3 _value;
    public override void OnInspectorGUI()
    {
        _value = EditorGUILayout.Vector3Field("Event Value", _value);
        if (GUILayout.Button("Execute Events"))
        {
            ((Vector3GameEvent)target).Invoke(_value);
        }
    }
}
