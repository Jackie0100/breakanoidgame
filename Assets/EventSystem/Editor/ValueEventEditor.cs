﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BoolRefEvent))]
public class ValueEventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Execute Events"))
        {
            ((BoolRefEvent)target).Value = ((BoolRefEvent)target).Value;
        }
    }
}
