﻿using UnityEngine;

[CreateAssetMenu(menuName = "Events/FloatGameEvent")]
public class FloatGameEvent : GameEvent<float>
{
}