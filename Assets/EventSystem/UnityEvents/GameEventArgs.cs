﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameEventArgs
{
}

public class GameEventArgs<T> : GameEventArgs
{
    public T Value { get; set; }

    public GameEventArgs(T value)
    {
        Value = value;
    }
}