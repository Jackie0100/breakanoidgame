﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayerEventListener : MonoBehaviour, IGameEventListener
{
    [SerializeField]
    SoundEffect _sound;
    [SerializeField]
    GameEvent _event;
    AudioSource _source;

    private void Start()
    {
        _source = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        _event.RegisterListener(this);
    }

    private void OnDisable()
    {
        _event.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        _source.PlayOneShot(_sound.Audio);
    }
}
