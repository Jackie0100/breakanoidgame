﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public sealed class GameEventListener : MonoBehaviour, IGameEventListener
{
    [SerializeField]
    GameEvent _event;
    [SerializeField]
    UnityEvent _response;

    private void OnEnable()
    {
        _event?.RegisterListener(this);
    }

    private void OnDisable()
    {
        _event?.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        _response.Invoke();
    }
}

public abstract class GameEventListener<T> : MonoBehaviour, IGameEventListener<T>
{
    public abstract GameEvent<T> GameEvent { get; }
    public abstract UnityEvent<T> Response { get; }

    private void OnEnable()
    {
        GameEvent?.RegisterListener(this);
    }

    private void OnDisable()
    {
        GameEvent?.UnregisterListener(this);
    }

    public void OnEventRaised(T value)
    {
        Response.Invoke(value);
    }
}