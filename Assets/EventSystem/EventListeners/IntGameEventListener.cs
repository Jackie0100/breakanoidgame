﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IntGameEventListener : GameEventListener<int>
{
    [SerializeField] private IntGameEvent _gameEvent;
    [SerializeField] private IntUnityEvent _response;

    public override GameEvent<int> GameEvent
    {
        get
        {
            return _gameEvent;
        }
    }

    public override UnityEvent<int> Response
    {
        get
        {
            return _response;
        }
    }
}
