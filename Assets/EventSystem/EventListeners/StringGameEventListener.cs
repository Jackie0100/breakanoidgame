﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StringGameEventListener : GameEventListener<string>
{
    [SerializeField] private StringGameEvent _gameEvent;
    [SerializeField] private StringUnityEvent _response;

    public override GameEvent<string> GameEvent
    {
        get
        {
            return _gameEvent;
        }
    }

    public override UnityEvent<string> Response
    {
        get
        {
            return _response;
        }
    }
}
