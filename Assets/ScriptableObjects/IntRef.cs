﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/IntRef")]
public class IntRef : ScriptableObject
{
    [SerializeField]
    protected int value;

    public virtual int Value
    {
        get
        {
            return value;
        }

        set
        {
            this.value = value;
        }
    }
}