﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/PowerUpCollection")]
public class PowerupCollection : ObservableCollectionRef<PowerUpSpawnContainer>
{
}
