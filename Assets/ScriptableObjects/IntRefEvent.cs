﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/IntRefEvent")]
public class IntRefEvent : IntRef
{
    [SerializeField]
    IntGameEvent _onValueChangedEvent;
    [SerializeField]
    public event IntRefValueChanged OnValueChanged;

    public override int Value
    {
        get
        {
            return base.Value;
        }
        set
        {
            base.Value = value;
            OnValueChanged?.Invoke(value);
            _onValueChangedEvent?.Invoke(value);
        }
    }
}

public delegate void IntRefValueChanged(int value);