﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/GameObjectCollection")]
public class GameObjectCollection : ObservableCollectionRef<GameObject>
{
}
