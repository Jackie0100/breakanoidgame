﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObservableCollectionRef<T> : ScriptableObject, ICollection<T>
{
    [SerializeField]
    private List<T> _value;
    [SerializeField]
    GameEvent _onValueChangedEvent;
    public event OnCollectionChanged OnValueChanged;

    int _previousCount;

    public void Reset()
    {
        _value = new List<T>();
    }

    public T this[int index]
    {
        get
        {
            return _value[index];
        }

        set
        {
            _value[index] = value;
            OnValueChanged?.Invoke();
            _onValueChangedEvent?.Invoke();
        }
    }

    public int Count
    {
        get
        {
            return _value.Count;
        }
    }

    public int PreviousCount
    {
        get
        {
            return _previousCount;
        }
    }

    public bool IsReadOnly
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public void Add(T value)
    {
        _previousCount = Count;
        _value.Add(value);
        OnValueChanged?.Invoke();
        _onValueChangedEvent?.Invoke();
    }

    public void Clear()
    {
        _previousCount = Count;
        _value.Clear();
        OnValueChanged?.Invoke();
        _onValueChangedEvent?.Invoke();
    }

    public bool Contains(T value)
    {
        return _value.Contains(value);
    }

    public void CopyTo(T[] array, int index)
    {
        _value.CopyTo(array, index);
    }

    //public IEnumerator GetEnumerator()
    //{
    //    return _value.GetEnumerator();
    //}

    public int IndexOf(T value)
    {
        return _value.IndexOf(value);
    }

    public void Insert(int index, T value)
    {
        _previousCount = Count;
        _value.Insert(index, value);
    }

    //public void Remove(T value)
    //{
    //    _value.Remove(value);
    //    OnValueChanged?.Invoke();
    //    _onValueChangedEvent?.Invoke();
    //}

    public void RemoveAt(int index)
    {
        _previousCount = Count;
        _value.RemoveAt(index);
        OnValueChanged?.Invoke();
        _onValueChangedEvent?.Invoke();
    }

    public bool Remove(T item)
    {
        _previousCount = Count;
        var isremoved = _value.Remove(item);
        if (isremoved)
        {
            OnValueChanged?.Invoke();
            _onValueChangedEvent?.Invoke();
        }
        return isremoved;
    }

    public IEnumerator<T> GetEnumerator()
    {
        return _value.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _value.GetEnumerator();
    }
}

public delegate void OnCollectionChanged();