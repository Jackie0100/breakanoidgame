﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sounds/SoundFxClip")]
public class SoundEffect : SimpleSoundEffect
{
    [SerializeField]
    [MinMaxRange(0, 1)]
    RangedFloat _volume = new RangedFloat() { MinValue = 1, MaxValue = 1 };
    [SerializeField]
    [MinMaxRange(-3, 3)]
    RangedFloat _pitch = new RangedFloat() { MinValue = 1, MaxValue = 1 };
    [SerializeField]
    bool _loop;

    public bool Loop
    {
        get
        {
            return _loop;
        }

        set
        {
            _loop = value;
        }
    }

    public RangedFloat Volume
    {
        get
        {
            return _volume;
        }

        set
        {
            _volume = value;
        }
    }

    public RangedFloat Pitch
    {
        get
        {
            return _pitch;
        }

        set
        {
            _pitch = value;
        }
    }

    public override void Play(AudioSource source)
    {
        if (Audio == null) return;

        source.clip = Audio;
        source.volume = Random.Range(Volume.MinValue, Volume.MaxValue);
        source.pitch = Random.Range(Pitch.MinValue, Pitch.MaxValue);
        source.PlayOneShot(Audio);
    }
}
