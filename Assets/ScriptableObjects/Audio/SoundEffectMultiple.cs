﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sounds/SoundFxClipMultiple")]
public class SoundEffectMultiple : AudioRef
{
    [SerializeField]
    AudioClip[] _audio;
    [SerializeField]
    [MinMaxRange(0, 1)]
    RangedFloat _volume = new RangedFloat() { MinValue = 1, MaxValue = 1 };
    [SerializeField]
    [MinMaxRange(-3, 3)]
    RangedFloat _pitch = new RangedFloat() { MinValue = 1, MaxValue = 1 };
    [SerializeField]
    bool _loop;

    public bool Loop
    {
        get
        {
            return _loop;
        }

        set
        {
            _loop = value;
        }
    }

    public RangedFloat Volume
    {
        get
        {
            return _volume;
        }

        set
        {
            _volume = value;
        }
    }

    public RangedFloat Pitch
    {
        get
        {
            return _pitch;
        }

        set
        {
            _pitch = value;
        }
    }

    public AudioClip[] Audio
    {
        get
        {
            return _audio;
        }

        set
        {
            _audio = value;
        }
    }

    public override void Play(AudioSource source)
    {
        if (Audio == null) return;

        var clip = Audio[Random.Range(0, Audio.Length)];
        source.clip = clip;
        source.volume = Random.Range(Volume.MinValue, Volume.MaxValue);
        source.pitch = Random.Range(Pitch.MinValue, Pitch.MaxValue);
        source.PlayOneShot(clip);
    }
}
