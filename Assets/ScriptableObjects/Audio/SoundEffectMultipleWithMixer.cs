﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "Sounds/SoundFxClipMultipleWithMixer")]
public class SoundEffectMultipleWithMixer : SoundEffectMultiple
{
    [SerializeField]
    AudioMixerGroup _audioMixer;

    public AudioMixerGroup AudioMixer
    {
        get
        {
            return _audioMixer;
        }
        set
        {
            _audioMixer = value;
        }
    }

    public override void Play(AudioSource source)
    {
        source.outputAudioMixerGroup = AudioMixer;
        base.Play(source);
    }
}
