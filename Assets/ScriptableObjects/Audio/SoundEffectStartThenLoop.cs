﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sounds/SoundFxStartThenLoop")]
public class SoundEffectStartThenLoop : AudioRef
{
    [SerializeField]
    AudioClip _startAudio;
    [SerializeField]
    AudioClip _loopAudio;
    [SerializeField]
    BoolRefEvent _loopHandler;

    AudioSource _targetSource;

    public override void Play(AudioSource source)
    {
        _targetSource = source;
        _targetSource.loop = true;
        _loopHandler.OnValueChanged += OnLoopingHandlerValueChanged;
        source.PlayOneShot(_startAudio);
        source.clip = _loopAudio;
        source.PlayDelayed(_startAudio.length);
    }

    private void OnLoopingHandlerValueChanged(bool value)
    {
        _targetSource.Stop();
        _targetSource.loop = value;
    }
}
