﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sounds/SoundFxCollection")]
public class SoundEffectCollection : AudioRef
{
    AudioRef[] _audioClips;

    public override void Play(AudioSource source)
    {
        _audioClips[Random.Range(0, _audioClips.Length)].Play(source);
    }
}
