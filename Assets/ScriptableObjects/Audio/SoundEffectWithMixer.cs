﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "Sounds/SoundFxClipWithMixer")]
public class SoundEffectWithMixer : SoundEffect
{
    [SerializeField]
    AudioMixerGroup _audioMixer;

    public AudioMixerGroup AudioMixer
    {
        get
        {
            return _audioMixer;
        }
        set
        {
            _audioMixer = value;
        }
    }

    public override void Play(AudioSource source)
    {
        if (Audio == null) return;

        source.clip = Audio;
        source.outputAudioMixerGroup = AudioMixer;
        source.volume = Random.Range(Volume.MinValue, Volume.MaxValue);
        source.pitch = Random.Range(Pitch.MinValue, Pitch.MaxValue);
        source.PlayOneShot(Audio);
    }
}
