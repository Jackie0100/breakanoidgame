﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sounds/SoundFxClipSimple")]
public class SimpleSoundEffect : AudioRef
{
    [SerializeField]
    AudioClip _audio;

    public AudioClip Audio
    {
        get
        {
            return _audio;
        }

        set
        {
            _audio = value;
        }
    }

    public override void Play(AudioSource source)
    {
        source.PlayOneShot(Audio);
    }
}
