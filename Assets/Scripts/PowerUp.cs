﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody2D))]
public class PowerUp : MonoBehaviour 
{
    [SerializeReference]
    GameEvent _event;
    [SerializeField]
    int _targetEventValue = 2;
    [SerializeField]
    Rigidbody2D _rigidbody2D;
    AudioSource _audioSource;

    [Header("Sounds")]
    [Space(16)]
    [SerializeField]
    AudioClip[] _spawnAudio;
    [SerializeField]
    AudioClip[] _collisionAudio;
    [SerializeField]
    AudioClip[] _pickupAudio;


    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _audioSource = GetComponent<AudioSource>();

        var temp = new Vector2(Random.Range(-30f, 30f), Random.Range(-1f, 30f));
        _rigidbody2D.velocity = temp.normalized * Random.Range(10, 30);

        _audioSource.PlayOneShot(_spawnAudio.GetRandom());
    }

    private void FixedUpdate()
    {
        _rigidbody2D.velocity += Vector2.down * Time.fixedDeltaTime * 10;

        if (_rigidbody2D.velocity.y < -20)
        {
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, -20);
        }
    }

    public void ExecutePowerUp()
    {
        if (_event != null)
        {
            if (_event is GameEvent<int> gei)
            {
                gei.Invoke(_targetEventValue);
            }
            else if (_event is GameEvent<bool> geb)
            {
                geb.Invoke(true);
            }
            else
            {
                _event.Invoke();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.AudioSource.PlayOneShot(_pickupAudio.GetRandom());
            ExecutePowerUp();
            Destroy(gameObject);
        }
        else
        {
            _audioSource.PlayOneShot(_collisionAudio.GetRandom());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.AudioSource.PlayOneShot(_pickupAudio.GetRandom());
            ExecutePowerUp();
            Destroy(gameObject);
        }
        else
        {
            _audioSource.PlayOneShot(_collisionAudio.GetRandom());
        }
    }
}