﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider2D))]
public class Brick : MonoBehaviour
{
    [SerializeField]
    protected FloatRef _gridSize;
    [SerializeField]
    protected GameEvent _brickBrokenEvent;
    protected Collider2D _collider;

    protected List<Collider2D> cols;
    [SerializeField]
    protected bool markforbreak = false;
    [SerializeField]
    protected FloatRef _points;
    [SerializeField]
    [Range(0,1)]
    protected float _spawnChance;
    [SerializeField]
    protected PowerupCollection _powerupCollection;


    [Header("Sounds")]
    [Space(16)]
    [SerializeField]
    protected AudioClip[] _breakSound;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            if (collision.gameObject.GetComponent<Ball>().IsExplosive)
            {
                ExplodedBrick();
            }
            else
            {
                BreakBrick();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            if (collision.gameObject.GetComponent<Ball>().IsExplosive)
            {
                ExplodedBrick();
            }
            else
            {
                BreakBrick();
            }
        }
    }

    [Button]
    public virtual void BreakBrick()
    {
        AudioSource.PlayClipAtPoint(_breakSound.GetRandom(), transform.position);
        Destroy(this.gameObject);
        _brickBrokenEvent.Invoke();
        _points.Add(10);

        if (ShouldDropPowerUp())
        {
            DropPowerUp();
        }
    }

    [Button]
    public virtual void ExplodedBrick()
    {
        if (!markforbreak)
        {
            GetComponent<AudioSource>().PlayOneShot(_breakSound.GetRandom());
            _points.Add(10);
            markforbreak = true;
            StartCoroutine(HandleBreakingEffects());
            if (ShouldDropPowerUp())
            {
                DropPowerUp();
            }
        }
    }

    [Button]
    public virtual bool ShouldDropPowerUp()
    {
        return (Random.value <= _spawnChance);
    }

    [Button]
    public virtual void DropPowerUp()
    {
        var chance = Random.value;
        var possiblePowerups = _powerupCollection.Where(t => t.SpawnChance >= chance).ToArray();
        Instantiate(possiblePowerups.GetRandom().PowerUp, transform.position, Quaternion.identity);
    }

    protected IEnumerator HandleBreakingEffects()
    {
        float timer = 0;
        markforbreak = true;

        Material shader = this.GetComponent<SpriteRenderer>().material;
        cols = Physics2D.OverlapAreaAll((Vector2)transform.position - (Vector2)_collider.bounds.size, (Vector2)transform.position + (Vector2)_collider.bounds.size, (1 << 9)).ToList();
        //Debug.DrawLine((Vector2)transform.position - (Vector2)_collider.bounds.size, (Vector2)transform.position + (Vector2)_collider.bounds.size, Color.cyan, 10);
        _collider.enabled = false;

        while (shader.GetFloat("_Fade") > 0.5f)
        {
            shader.SetFloat("_Fade", 1 - Mathf.Lerp(0, 1, timer));
            timer += Time.deltaTime * 2;
            yield return new WaitForEndOfFrame();
        }

        if (this is ExplosiveBrick)
        {
            foreach (var col in cols)
            {
                if (col == null)
                    continue;
                col.GetComponent<Brick>()?.ExplodedBrick();
            }
        }

        while (shader.GetFloat("_Fade") > 0)
        {
            shader.SetFloat("_Fade", 1 - Mathf.Lerp(0, 1, timer));
            timer += Time.deltaTime * 2;
            yield return new WaitForEndOfFrame();
        }

        _brickBrokenEvent.Invoke();
        if (this.gameObject != null)
        {
            Destroy(this.gameObject);
        }
    }
}
