﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ExplosiveBrick : Brick
{
    [Button]
    public override void BreakBrick()
    {
        ExplodedBrick();
        _points.Add(10);
    }

    public void Detonate()
    {
        BreakBrick();
    }

    public void ExpandExplosion()
    {
        cols = Physics2D.OverlapAreaAll((Vector2)transform.position - (Vector2)_collider.bounds.size, (Vector2)transform.position + (Vector2)_collider.bounds.size, (1 << 9)).ToList();

        foreach (var col in cols)
        {
            if (col.GetComponent<ExplosiveBrick>() != null)
                continue;

            Instantiate(this.gameObject, col.transform.position, Quaternion.identity);
            Destroy(col.gameObject);
        }
    }
}
