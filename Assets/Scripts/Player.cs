﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(SpriteRenderer))]
public class Player : MonoBehaviour
{
    [SerializeField]
    CameraHelper _cameraHelper;
    [SerializeField]
    SpriteRenderer _playerSprite;
    [SerializeField]
    SpriteRenderer _gripSprite;
    [SerializeField]
    BallList _activeBalls;
    [SerializeField]
    GameObject _ball;
    [SerializeField]
    private bool _hasGripPower;
    [SerializeField]
    private bool _hasShootingPower;
    [SerializeField]
    List<Transform> _shooters;
    [SerializeField]
    private GameObject _projectile;
    [SerializeField]
    IntRef _playerLifes;
    [SerializeField]
    float _shrinkExpandAmount = 2.5f;
    [SerializeField]
    float _playerStartSize = 10;
    [SerializeField]
    float _minShrinkSize = 5;
    [SerializeField]
    float _maxExpandSize = 50;

    [SerializeField]
    Transform _leftShooter;
    [SerializeField]
    Transform _rightShooter;

    [Header("Sounds")]
    [Space(16)]
    [SerializeField]
    private AudioClip[] _deathSounds;
    [SerializeField]
    private AudioClip[] _ballLostSounds;
    [SerializeField]
    private AudioClip[] _gripSounds;
    [SerializeField]
    private AudioClip[] _shootingSounds;

    public AudioSource AudioSource { get; private set; }

    Vector3 _pastLocation;
    const float GRABOFFFSET = 0.2f;

    public bool HasGripPower
    {
        get
        {
            return _hasGripPower;
        }
        set
        {
            _hasGripPower = value;
        }
    }

    public bool HasShootingPower
    {
        get
        {
            return _hasShootingPower;
        }

        set
        {
            _hasShootingPower = value;
        }
    }

    private void Awake()
    {
        _activeBalls.Reset();
    }

    // Start is called before the first frame update
    void Start()
    {
        _pastLocation = transform.position;
        if (_cameraHelper == null)
        {
            _cameraHelper = Camera.main.GetComponent<CameraHelper>();
            Debug.LogWarning("Camera is not setup for the player! Auto applying camera from main...");
        }
        _playerSprite = GetComponent<SpriteRenderer>();
        AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, -_cameraHelper.TargetSize + (_playerSprite.bounds.size.x * 0.5f), _cameraHelper.TargetSize - (_playerSprite.bounds.size.x * 0.5f)), transform.position.y, 0);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //Todo: Optimized with a list of held balls to avoid looping over all of them.
            foreach (Ball ball in _activeBalls)
            {
                if (ball.IsBallHeld)
                {
                    ball.LaunchBall(this);
                }
            }

            if (HasShootingPower)
            {
                FireProjectiles();
            }
        }
        foreach (Ball ball in _activeBalls)
        {
            if (ball.IsBallHeld)
            {
                ball.transform.position += transform.position - _pastLocation;
            }
        }

        _pastLocation = transform.position;
    }

    [Button]
    private void FireProjectiles()
    {
        foreach (var shooter in _shooters)
        {
            GameObject.Instantiate(_projectile, shooter.transform.position, Quaternion.identity);
        }
        AudioSource.PlayOneShot(_shootingSounds.GetRandom());
    }

    public void OnBallLost()
    {
        if (_activeBalls.Count == 0)
        {
            _playerLifes.Value--;
            //RespawnBall();
            AudioSource.PlayOneShot(_deathSounds.GetRandom());
        }
        else if (_activeBalls.PreviousCount > _activeBalls.Count)
        {
            AudioSource.PlayOneShot(_ballLostSounds.GetRandom());
        }
    }

    public void PlaceBallCorrect(Ball ball)
    {
        AudioSource.PlayOneShot(_gripSounds.GetRandom());
        ball.IsBallHeld = true;
        ball.transform.position = new Vector3(ball.transform.position.x, transform.position.y + 0.875f);
    }

    public void RespawnBall()
    {
        Instantiate(_ball, new Vector3(transform.position.x, transform.position.y + 0.975f, 0), Quaternion.identity);
    }

    public void LengthenPlayer()
    {
        _playerSprite.size = new Vector2(Mathf.Clamp(_playerSprite.size.x + _shrinkExpandAmount, _minShrinkSize, _maxExpandSize), _playerSprite.size.y);
        _gripSprite.size = new Vector2(_playerSprite.size.x - GRABOFFFSET, _gripSprite.size.y);
        _leftShooter.localPosition = new Vector3(_playerSprite.size.x * -0.5f, _leftShooter.localPosition.y, 0);
        _rightShooter.localPosition = new Vector3(_playerSprite.size.x * 0.5f, _rightShooter.localPosition.y, 0);
    }

    public void ShortenPlayer()
    {
        _playerSprite.size = new Vector2(Mathf.Clamp(_playerSprite.size.x - _shrinkExpandAmount, _minShrinkSize, _maxExpandSize), _playerSprite.size.y);
        _gripSprite.size = new Vector2(_playerSprite.size.x - GRABOFFFSET, _gripSprite.size.y);
        _leftShooter.localPosition = new Vector3(_playerSprite.size.x * -0.5f, _leftShooter.localPosition.y, 0);
        _rightShooter.localPosition = new Vector3(_playerSprite.size.x * 0.5f, _rightShooter.localPosition.y, 0);
    }

    public void ResetPlayerSize()
    {
        _playerSprite.size = new Vector2(_playerStartSize, _playerSprite.size.y);
        _gripSprite.size = new Vector2(_playerSprite.size.x - GRABOFFFSET, _gripSprite.size.y);
    }

    public void SetSmallestPlayer()
    {
        _playerSprite.size = new Vector2(_minShrinkSize, _playerSprite.size.y);
        _gripSprite.size = new Vector2(_playerSprite.size.x - GRABOFFFSET, _gripSprite.size.y);
    }
}
