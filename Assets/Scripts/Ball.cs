﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Ball : MonoBehaviour
{
    [SerializeField]
    float _movementMultiplier = 1.01f;
    [SerializeField]
    float _minMovementSpeed = 1;
    [SerializeField]
    float _maxMovementSpeed = 8;
    [SerializeField]
    float _controlPrecisionMultiplier = 5;
    [SerializeField]
    float _randomturning = 2;
    [SerializeField]
    private bool _isBallHeld;
    [SerializeField]
    private bool _isExplosive;
    [SerializeField]
    private bool _isBullDozer;
    [SerializeField]
    FloatRef _bottomOfScreen;
    [SerializeField]
    FloatRef _maxAngle;
    [SerializeField]
    BallList _activeBalls;
    [SerializeField]
    GameEvent _playerDeath;

    bool _isFirstShot = true;

    [Header("Sounds")]
    [Space(16)]
    [SerializeField]
    private AudioClip[] _playerHitSound;
    [SerializeField]
    private AudioClip[] _collisionWallSound;

    public bool IsBallHeld
    {
        get
        {
            return _isBallHeld;
        }
        set
        {
            _isBallHeld = value;
        }
    }

    public bool IsExplosive
    {
        get
        {
            return _isExplosive;
        }
        set
        {
            _isExplosive = value;
        }
    }

    public bool IsBullDozer
    {
        get
        {
            return _isBullDozer;
        }
        set
        {
            _isBullDozer = value;
        }
    }

    float _speed = 1;

    Rigidbody2D _rigidbody;
    SpriteRenderer _spriteRenderer;
    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _speed = _minMovementSpeed;
        _rigidbody = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _activeBalls.Add(this);
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckShouldBeDestroyed())
        {
            _activeBalls.Remove(this);
            Destroy(this.gameObject);
            if (_activeBalls.Count == 0)
                _playerDeath.Invoke();
        }

        Vector2 dir = _rigidbody.velocity;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        if ((Mathf.Abs(angle) < _maxAngle.Value || Mathf.Abs(angle) > 180 - _maxAngle.Value) && _rigidbody.velocity != Vector2.zero)
        {
            Debug.Log("Correcting Ball angle");
            float velX = 0;
            float velY = 0;
            if (_rigidbody.velocity.x < 0)
                velX = _rigidbody.velocity.x * Mathf.Cos(_maxAngle.Value);
            else if (_rigidbody.velocity.x > 0)
                velX = _rigidbody.velocity.x * Mathf.Cos(_maxAngle.Value);

            if (_rigidbody.velocity.y < 0)
                velY = _rigidbody.velocity.y * Mathf.Sin(-_maxAngle.Value);
            else if (_rigidbody.velocity.y > 0)
                velY = _rigidbody.velocity.y * Mathf.Sin(_maxAngle.Value);

            _rigidbody.velocity = new Vector2(velX, velY).normalized * _rigidbody.velocity.magnitude;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<Player>() != null)
            {
                if (collision.gameObject.GetComponent<Player>().HasGripPower)
                {
                    IsBallHeld = true;
                    _rigidbody.velocity = Vector2.zero;
                    collision.gameObject.GetComponent<Player>().PlaceBallCorrect(this);
                }
                else
                {
                    var relativeloc = transform.InverseTransformPoint(collision.transform.position);
                    var halfsize = collision.gameObject.GetComponent<SpriteRenderer>().bounds.size * 0.5f;
                    var nomalizedball = relativeloc.x / halfsize.x;
                    _speed = Mathf.Clamp(_speed * _movementMultiplier, _minMovementSpeed, _maxMovementSpeed);
                    var targetvelocity = _rigidbody.velocity + new Vector2((-nomalizedball * _controlPrecisionMultiplier) + Random.Range(-_randomturning, _randomturning), 0);
                    _rigidbody.velocity = _speed * targetvelocity.normalized;
                }
            }
            _audioSource.PlayOneShot(_playerHitSound.GetRandom());
        }
        else if (collision.gameObject.tag == "Wall")
        {
            _audioSource.PlayOneShot(_collisionWallSound.GetRandom());
        }
    }

    public bool CheckShouldBeDestroyed()
    {
        return (transform.position.y < _bottomOfScreen.Value - _spriteRenderer.bounds.size.y);
    }

    public void LaunchBall(Player player)
    {
        if (IsBallHeld)
        {
            IsBallHeld = false;
            if (_isFirstShot)
            {
                _rigidbody.velocity = new Vector2(Random.Range(-_minMovementSpeed, _minMovementSpeed), _minMovementSpeed);
                _isFirstShot = false;
            }
            else
            {
                var relativeloc = transform.InverseTransformPoint(player.transform.position);
                var halfsize = player.GetComponent<SpriteRenderer>().bounds.size * 0.5f;
                var nomalizedball = relativeloc.x / halfsize.x;

                _speed = Mathf.Clamp(_speed * _movementMultiplier, _minMovementSpeed, _maxMovementSpeed);
                var targetvelocity = _rigidbody.velocity + new Vector2((-nomalizedball * _controlPrecisionMultiplier) + Random.Range(-_randomturning, _randomturning), 0);
                _rigidbody.velocity = _speed * -targetvelocity.normalized;
            }
        }
    }

    public void SplitBall(int count)
    {
        Vector2 dir = _rigidbody.velocity;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        switch (count)
        {
            case 1:
                SpawnOneBall();
                break;
            case 2:
                SpawnTwoBall();
                break;
            case 3:
                SpawnThreeBall();
                break;
            case 7:
                SpawnSevenBall();
                break;
            default:
                break;
        }
    }

    private void SpawnOneBall()
    {
        var go = GameObject.Instantiate(this.gameObject);
        var ball = go.GetComponent<Ball>();
        var rb = ball.GetComponent<Rigidbody2D>();
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(-_rigidbody.velocity.x, _rigidbody.velocity.y);
    }

    private void SpawnTwoBall()
    {
        SpawnOneBall();
        var go = GameObject.Instantiate(this.gameObject);
        var ball = go.GetComponent<Ball>();
        var rb = ball.GetComponent<Rigidbody2D>();
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(0, _rigidbody.velocity.y).normalized * _rigidbody.velocity.magnitude;
    }

    private void SpawnThreeBall()
    {
        SpawnOneBall();
        var go = GameObject.Instantiate(this.gameObject);
        var ball = go.GetComponent<Ball>();
        var rb = ball.GetComponent<Rigidbody2D>();
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(-_rigidbody.velocity.x, -_rigidbody.velocity.y);

        go = GameObject.Instantiate(this.gameObject);
        ball = go.GetComponent<Ball>();
        rb = ball.GetComponent<Rigidbody2D>();
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(_rigidbody.velocity.x, -_rigidbody.velocity.y);
    }

    private void SpawnSevenBall()
    {
        Vector2 dir = _rigidbody.velocity;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        
        for (int i = 0; i < 8; i++)
        {
            angle += 45;
            var go = GameObject.Instantiate(this.gameObject);
            var ball = go.GetComponent<Ball>();
            var rb = ball.GetComponent<Rigidbody2D>();
            ball.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * _rigidbody.velocity.magnitude;
        }
    }

    public void SlowBall()
    {
        if (_speed > _minMovementSpeed + (_minMovementSpeed * 0.05f))
        {
            _speed = _minMovementSpeed + (_minMovementSpeed * 0.05f);
            _rigidbody.velocity = _speed * _rigidbody.velocity.normalized;
        }
    }

    public void FastBall()
    {
        if (_speed < _maxMovementSpeed - (_maxMovementSpeed * 0.05f))
        {
            _speed = _maxMovementSpeed - (_maxMovementSpeed * 0.05f);
            _rigidbody.velocity = _speed * _rigidbody.velocity.normalized;
        }
    }

    public void SmallBall()
    {

    }

    public void BigBall()
    {

    }

    public void ResetBallSize()
    {

    }
}
