﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CompletionChecker : MonoBehaviour
{
    [SerializeField]
    GameEvent _levelCompletedEvent;

    public void CheckLevelCompleted()
    {
        if (this.transform.childCount == 0)
        {
            _levelCompletedEvent.Invoke();
        }
    }
}
