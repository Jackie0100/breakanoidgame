﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PowerUpSpawnContainer
{
    [SerializeField]
    GameObject _powerUp;
    [SerializeField]
    [Range(0,1)]
    float _spawnChance;

    public GameObject PowerUp
    {
        get
        {
            return _powerUp;
        }
        private set
        {
            _powerUp = value;
        }
    }

    public float SpawnChance
    {
        get
        {
            return _spawnChance;
        }
        private set
        {
            _spawnChance = value;
        }
    }
}
