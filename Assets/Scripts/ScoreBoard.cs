﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreBoard : MonoBehaviour
{
    Text _scoreText;
    float _targetScore;
    float _startingScore = 0;
    float _currentScore;
    float _time;

    [SerializeField]
    [Range(0.1f, 10)]
    float _countSeconds;
    // Start is called before the first frame update
    void Start()
    {
        _scoreText = GetComponent<Text>();
    }

    private void Update()
    {
        _currentScore = Mathf.Lerp(_startingScore, _targetScore, _time);
        _time += (Time.deltaTime / _countSeconds);
        _scoreText.text = _currentScore.ToString("00000000");
    }

    public void SetScore(float score)
    {
        _targetScore = score;
        _startingScore = _currentScore;

        _time = 0;
    }
}
