﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraHelper : MonoBehaviour
{
    [SerializeField]
    float _targetSize = 10;
    float _borderThickness = 1;
    Camera _cam;
    [SerializeField]
    GameObject _leftBorder, _topBorder, _rightBorder;
    [SerializeField]
    FloatRef _bottomOfScreen;

    public float TargetSize
    {
        get { return _targetSize; }
    }

    // Start is called before the first frame update
    void Start()
    {
        _cam = GetComponent<Camera>();
        _cam.orthographicSize = TargetSize / (16.0f / 9.0f);

        _leftBorder = new GameObject();
        BoxCollider2D boxleft = _leftBorder.AddComponent<BoxCollider2D>();
        boxleft.size = new Vector2(_borderThickness, _cam.orthographicSize * 2);
        _leftBorder.transform.position = new Vector3(-TargetSize - (_borderThickness * 0.5f), _cam.transform.position.y);
        _leftBorder.transform.SetParent(transform);
        _leftBorder.name = "LeftWall";
        _leftBorder.layer = 11;
        _leftBorder.tag = "Wall";

        _rightBorder = new GameObject();
        BoxCollider2D boxright = _rightBorder.AddComponent<BoxCollider2D>();
        boxright.size = new Vector2(_borderThickness, _cam.orthographicSize * 2);
        _rightBorder.transform.position = new Vector3(TargetSize + (_borderThickness * 0.5f), _cam.transform.position.y);
        _rightBorder.transform.SetParent(transform);
        _rightBorder.name = "RightWall";
        _rightBorder.layer = 11;
        _rightBorder.tag = "Wall";

        _topBorder = new GameObject();
        BoxCollider2D boxtop = _topBorder.AddComponent<BoxCollider2D>();
        boxtop.size = new Vector2((TargetSize * 2) + (_borderThickness * 2), _borderThickness);
        _topBorder.transform.position = new Vector3(_cam.transform.position.x, _cam.orthographicSize + (_borderThickness * 0.5f));
        _topBorder.transform.SetParent(transform);
        _topBorder.name = "TopWall";
        _topBorder.layer = 11;
        _topBorder.tag = "Wall";

        _bottomOfScreen.Value = -_cam.orthographicSize;
    }
}
