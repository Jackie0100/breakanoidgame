﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasFader : MonoBehaviour
{
    Coroutine _coroutine;
    CanvasGroup _canvasGroup;

    private void Start()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void FadeIn(float time)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = StartCoroutine(FadeInHandler(time));
    }

    public void FadeOut(float time)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = StartCoroutine(FadeOutHandler(time));
    }

    public void FadeInOut(float time)
    {
        _coroutine = StartCoroutine(FadeOutHandler(time));
    }

    public void FadeOutWaitIn(float time)
    {
    }

    IEnumerator FadeOutHandler(float time)
    {
        float timer = 0;
        _canvasGroup.interactable = false;
        while (timer < time)
        {
            _canvasGroup.alpha = Mathf.Lerp(1, 0, timer / time);
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _canvasGroup.blocksRaycasts = false;
    }

    IEnumerator FadeInHandler(float time)
    {
        float timer = 0;
        _canvasGroup.blocksRaycasts = true;
        while (timer < time)
        {
            _canvasGroup.alpha = Mathf.Lerp(0, 1, timer / time);
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _canvasGroup.interactable = true;
    }

    IEnumerator FadeInOutHandler(float time)
    {
        float timer = 0;
        _canvasGroup.blocksRaycasts = true;
        while (timer < time)
        {
            _canvasGroup.alpha = Mathf.Lerp(0, 1, timer / time);
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        timer = 0;
        _canvasGroup.interactable = true;
    }

}
