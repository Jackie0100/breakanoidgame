﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static T GetRandom<T>(this T[] arr)
    {
        if (arr != null && arr.Length > 0)
        {
            return arr[UnityEngine.Random.Range(0, arr.Length)];
        }
        return default(T);
    }
}
