﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeDisplay : MonoBehaviour
{
    [SerializeField]
    float _imageSize;
    [SerializeField]
    IntRef _life;
    [SerializeField]
    Sprite _uiImage;

    private void Start()
    {
        SetLifeDisplay(_life.Value);
    }

    public void SetLifeDisplay(int value)
    {
        if (value > transform.childCount)
        {
            int c = value - transform.childCount;

            for (int i = 0; i < c; i++)
            {
                GameObject go = new GameObject();
                var sr = go.AddComponent<Image>();
                sr.sprite = _uiImage;
                sr.preserveAspect = true;
                go.GetComponent<RectTransform>().sizeDelta = new Vector2(_imageSize, _imageSize);
                go.transform.SetParent(transform);
            }
        }

        if (value < transform.childCount)
        {
            int c = (int)Mathf.Clamp(transform.childCount - value, 0, transform.childCount);

            for (int i = 0; i < c; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }
}
