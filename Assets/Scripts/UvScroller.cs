﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UvScroller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<SpriteRenderer>().material.mainTextureOffset = new Vector2(Time.time, 0);
    }
}
